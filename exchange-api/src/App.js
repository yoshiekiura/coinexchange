import React from 'react';
import './App.css';
// import path from 'path';
// import YAML from "yamljs";

import SwaggerUI from "swagger-ui-react"
import "swagger-ui-react/swagger-ui.css"


function App() {
  // const swaggerDocument = YAML.load(path.join(__dirname, "/coinexchange//swagger.yaml"));
  const swaggerUrl = "/coinexchange/swagger.yaml";

  return (
    <div className="App">

      {/* <SwaggerUI spec={swaggerDocument}></SwaggerUI> */}
      <SwaggerUI url={swaggerUrl}></SwaggerUI>
      
    </div>
  );
}

export default App;
