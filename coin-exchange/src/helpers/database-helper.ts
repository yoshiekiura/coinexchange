import nodeSqlite3 from "sqlite3";
import ENV from "../util/env";

class DatabaseHelper {
  static get Instance() {
    return this.instance || (this.instance = new this());
  }
  get DB(): nodeSqlite3.Database {
    if (!this.db) {
      // const sqlite3 = (ENV.isDev()) ? nodeSqlite3.verbose() : nodeSqlite3;
      const sqlite3 = nodeSqlite3.verbose();
      this.db = new sqlite3.Database(":memory:");
    }
    return this.db;
  }

  private static instance: DatabaseHelper;

  private db: nodeSqlite3.Database;

  private constructor() { }

  // temporary
  public close() {
    if (this.db) {
      // this.db.close();
      // this.db = null;
    }
  }

}

export default DatabaseHelper;
