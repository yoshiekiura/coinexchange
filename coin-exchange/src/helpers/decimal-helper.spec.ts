import { expect } from "chai";
import "mocha";

import DecimalHelper from "./decimal-helper";

describe("test adjustPrecision function", () => {

  it("should tuncate to 2 digits below zero", () => {
    const amount1 = 34.5678;
    const expected1 = 34.56;
    const result1 = DecimalHelper.adjustPrecision(amount1)
    expect(result1).to.equal(expected1);

    const amount2 = -34.5678;
    const expected2 = -34.56;
    const result2 = DecimalHelper.adjustPrecision(amount2)
    expect(result2).to.equal(expected2);
  });

  it("should tuncate to 3 digits below zero", () => {
    const amount1 = 34.1234567890123;
    const expected1 = 34.123;
    const result1 = DecimalHelper.adjustPrecision(amount1, 3)
    expect(result1).to.equal(expected1);

    const amount2 = -34.1234567890123;
    const expected2 = -34.123;
    const result2 = DecimalHelper.adjustPrecision(amount2, 3)
    expect(result2).to.equal(expected2);
  });
});


describe("test isValidPrecision function", () => {

  it("should check precision to be correct", () => {
    const amount1 = 34.56;
    const result1 = DecimalHelper.isValidPrecision(amount1)
    expect(result1).to.true;

    const amount2 = 34;
    const result2 = DecimalHelper.isValidPrecision(amount2)
    expect(result2).to.true;
  });

  it("should check precision to be incorrect", () => {
    const amount1 = 34.5678;
    const result1 = DecimalHelper.isValidPrecision(amount1, 3)
    expect(result1).to.false;

    const amount2 = -34.123456;
    const result2 = DecimalHelper.isValidPrecision(amount2)
    expect(result2).to.false;
  });
});
