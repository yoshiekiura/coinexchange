class DecimalHelper {

  // For our use cases, amount >= 0.
  // Hence there is really no difference, but we do truncate not round.
  public static adjustPrecision(amount: number, digitsBelowZero: number = 2): number {

    const factor = Math.pow(10, digitsBelowZero);
    const mul = amount * factor;
    if ((Math.abs(mul) >= 0 && mul <= Number.MAX_SAFE_INTEGER)
      || (Math.abs(mul) < 0 && mul >= Number.MIN_SAFE_INTEGER)) {
      const trunc = Math.trunc(mul);
      const adjusted =  trunc / factor;
      return adjusted;
    } else {
      const wholeNumber = Math.trunc(amount);
      const numBelowZero = amount - wholeNumber;
      const mulBleowZero = numBelowZero * factor;
      const truncBelowZero = Math.trunc(mulBleowZero);
      const adjusted = wholeNumber + truncBelowZero / factor;
      return adjusted;
    }
  }

  public static isValidPrecision(amount: number, digitsBelowZero: number = 2): boolean {
    const adjusted = DecimalHelper.adjustPrecision(amount, digitsBelowZero);
    return (Math.abs(amount - adjusted) < Number.EPSILON);
  }

  public static multiplyWithPrecision(amount1: number, amount2: number, digitsBelowZero: number = 2): number {
    return DecimalHelper.adjustPrecision(amount1 * amount2, digitsBelowZero);
  }

  public static inverseWithPrecision(amount: number, digitsBelowZero: number = 2): number {
    // amount > 0.
    return DecimalHelper.adjustPrecision(1 / amount, digitsBelowZero);
  }

  public static isZero(amount: number, digitsBelowZero: number = 2): boolean {
    return (Math.abs(DecimalHelper.adjustPrecision(amount, digitsBelowZero)) < Number.EPSILON);
  }

  public static isEqual(amount1: number, amount2: number, digitsBelowZero: number = 2): boolean {
    return DecimalHelper.isZero(amount1 - amount2, digitsBelowZero);
  }

  public static isPositive(amount: number, digitsBelowZero: number = 2): boolean {
    return (DecimalHelper.adjustPrecision(amount, digitsBelowZero) >= Number.EPSILON);
  }

  private constructor() { }

}

export default DecimalHelper;
