import QueryManager from "../db/query-manager";
import TransactionRecord from "../models/transaction-record";

export function getTransaction(id: string, cb: (record: TransactionRecord) => void) {
  QueryManager.getTransaction(id, (result: {}) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const record = TransactionRecord.makeTransactionRecord(result);
        cb(record);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getTransaction() - callback is null.");
    }
  });
}

export function getUserTransactions(uid: string, cb: (records: TransactionRecord[]) => void) {
  QueryManager.getUserTransactions(uid, (result: any[]) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const records: TransactionRecord[] = [];
        for (const r of result) {
          const record = TransactionRecord.makeTransactionRecord(r);
          records.push(record);
        }
        cb(records);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getUserTransactions() - callback is null.");
    }
  });
}
