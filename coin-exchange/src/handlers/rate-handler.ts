import CoinCurrency from "../common/core/coin-currency";
import QueryManager from "../db/query-manager";
import DecimalHelper from "../helpers/decimal-helper";
import ExchangeRate from "../models/exchange-rate";
import TransactionRecord from "../models/transaction-record";

export function getRate(targetCurrency: CoinCurrency,
                        baseCurrency: CoinCurrency,
                        cb: (rate: ExchangeRate) => void) {

  QueryManager.getLatestTransactionForCurrencies(targetCurrency, baseCurrency, (result: any) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const record = TransactionRecord.makeTransactionRecord(result);
        let rate = 1.0;   // Default/starting rate is always 1.
        if (record.buyValue.currency === targetCurrency
          && DecimalHelper.isPositive(record.sellValue.amount)) {
          rate = DecimalHelper.adjustPrecision(record.buyValue.amount / record.sellValue.amount);
        } else if (record.sellValue.currency === targetCurrency
          && DecimalHelper.isPositive(record.buyValue.amount)) {
          rate = DecimalHelper.adjustPrecision(record.sellValue.amount / record.buyValue.amount);
        }
        const exchangeRate = new ExchangeRate(
          targetCurrency,
          baseCurrency,
          rate,
          record.completedAt
        );
        cb(exchangeRate);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getRate() - callback is null.");
    }
  });
}
