import CoinCurrency from "../common/core/coin-currency";
import CoinMoney from "../models/coin-money";
import UserWallet from "../models/user-wallet";

// Placeholder for now.
export function getBalance(): UserWallet {

  // tbd:
  const record = new UserWallet("0", [new CoinMoney(CoinCurrency.XLM, 2.5)]);
  return record;
}
