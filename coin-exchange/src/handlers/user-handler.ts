import InsertManager from "../db/insert-manager";
import QueryManager from "../db/query-manager";
import CoinMoney from "../models/coin-money";
import UserWallet from "../models/user-wallet";

export function getWallet(uid: string, cb: (record: UserWallet) => void) {

  QueryManager.getUserWallet(uid, (result: {}) => {
    console.log("result = ", result);
    if (cb) {
      if (result) {
        const record = UserWallet.makeUserWallet(result);
        cb(record);
      } else {
        cb(null);
      }
    } else {
      console.log("WARNING: getUserWallet() - callback is null.");
    }
  });
}

export function addToWallet(uid: string, deposit: CoinMoney, cb: (record: UserWallet) => void) {
  InsertManager.addToUserWallet(uid, deposit.currency, deposit.amount, (suc: boolean) => {
    console.log(`suc = ${suc}`);
    if (suc) {
      getWallet(uid, cb);
    } else {
      if (cb) {
        cb(null);
      } else {
        console.log("WARNING: addToWallet() - callback is null.");
      }
    }
  });
}
