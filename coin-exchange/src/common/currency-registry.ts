import CoinCurrency from "./core/coin-currency";

class CurrencyRegsitry {

  public static getAll(): CoinCurrency[] {
    const list: CoinCurrency[] = Object.keys(CoinCurrency).map((k) => (k as CoinCurrency));
    return list;
  }

  public static isValid(currency: string): boolean {
    const found = Object.keys(CoinCurrency).find((c) => c === currency);
    return (found) ? true : false;
  }

  private constructor() { }

}

export default CurrencyRegsitry;
