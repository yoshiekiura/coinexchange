import { expect } from "chai";
import "mocha";
import CoinCurrency from "./core/coin-currency";
import CurrencyRegsitry from "./currency-registry";

describe("test currencies", () => {

  it("should return all supported currencies", () => {
    const result = CurrencyRegsitry.getAll();
    for (const cur in CoinCurrency) {
      expect(result).to.include(cur);
    }
  });

  it("should be able to check valid currencies", () => {
    for (const cur in CoinCurrency) {
      const truthy = CurrencyRegsitry.isValid(cur);
      expect(truthy).to.true;
    }
    for (const str in ['ABC', 'XYZ']) {
      const falsey = CurrencyRegsitry.isValid(str);
      expect(falsey).to.false;
    }
  });

});
