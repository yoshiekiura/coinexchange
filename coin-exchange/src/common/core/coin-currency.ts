enum CoinCurrency {
  USD = "USD",
  BTC = "BTC",
  ETH = "ETH",
  RBX = "RBX",
  XLM = "XLM",
}

export default CoinCurrency;
