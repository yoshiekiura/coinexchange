enum OrderStatus {
  Open = 1,
  Completed = 2,
  // Partial = 3,
}

export default OrderStatus;
