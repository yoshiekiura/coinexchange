import Joi from "joi";
import CoinMoney from "./coin-money";

class UserWallet {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    uid: Joi.string().alphanum().min(1).max(64).required(),
    balance: Joi.array(),
  });

  // Factory method.
  public static makeUserWallet(obj: any): UserWallet {
    const cloned = Object.assign({}, obj) as UserWallet;
    const list: CoinMoney[] = [];
    for (const b of obj.balance) {
      list.push(CoinMoney.makeCoinMoney(b));
    }
    cloned.balance = list;
    return cloned;
  }

  constructor(public uid: string, public balance: CoinMoney[]) {
    this.balance = balance || [];
  }

  // public addMoney(money: CoinMoney) {
  //   // tbd
  //   console.log(`money = ${money}`);
  // }

  // public removeMoney(money: CoinMoney) {
  //   // tbd
  //   console.log(`money = ${money}`);
  // }

  // public getBalance(currency: CoinCurrency): CoinMoney {
  //   // tbd
  //   return new CoinMoney(CoinCurrency.BTC, 0);
  // }

}

export default UserWallet;
