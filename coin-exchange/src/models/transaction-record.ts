import Joi from "joi";
import CoinMoney from "./coin-money";

class TransactionRecord {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    id: Joi.string().alphanum().min(1).max(64).required(),
    buyOrderId: Joi.string().alphanum().min(1).max(64).required(),
    sellOrderId: Joi.string().alphanum().min(1).max(64).required(),
    buyer: Joi.string().alphanum().min(1).max(64).required(),
    seller: Joi.string().alphanum().min(1).max(64).required(),
    buyValue: Joi.object().keys({
      currency: Joi.string().alphanum().min(3).max(3).required(),
      amount: Joi.number().required()
    }),
    sellValue: Joi.object().keys({
      currency: Joi.string().alphanum().min(3).max(3).required(),
      amount: Joi.number().required()
    }),
    completedAt: Joi.date(),
  });

  // Factory method.
  public static makeTransactionRecord(obj: any): TransactionRecord {
    const cloned = Object.assign({}, obj) as TransactionRecord;
    cloned.buyValue = CoinMoney.makeCoinMoney(obj.buyValue);
    cloned.sellValue = CoinMoney.makeCoinMoney(obj.sellValue);
    return cloned;
  }

  constructor(
    public id: string,
    public buyOrderId: string,
    public sellOrderId: string,
    public buyer: string,
    public seller: string,
    public buyValue: CoinMoney,
    public sellValue: CoinMoney,
    public completedAt: Date) {
  }

}

export default TransactionRecord;
