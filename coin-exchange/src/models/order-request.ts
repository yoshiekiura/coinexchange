import Joi from "joi";
import CoinCurrency from "../common/core/coin-currency";
import OrderSide from "../common/core/order-side";
import CoinMoney from "./coin-money";
import OrderVariety from "./order-variety";

class OrderRequest {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    uid: Joi.string().alphanum().min(1).max(64).required(),
    side: Joi.number().required(),
    variety: Joi.object().keys({
      orderType: Joi.number().required(),
      rateLimit: Joi.number(),
    }),
    target: Joi.object().keys({
      currency: Joi.string().alphanum().min(3).max(3).required(),
      amount: Joi.number().required()
    }),
    exchangeCurrency: Joi.string().alphanum().min(3).max(3).required(),
  });

  // Factory method.
  public static makeOrderRequest(obj: any): OrderRequest {
    const cloned = Object.assign({}, obj) as OrderRequest;
    cloned.variety = OrderVariety.makeOrderVariety(obj.variety);
    cloned.target = CoinMoney.makeCoinMoney(obj.target);
    return cloned;
  }

  constructor(
    public uid: string,
    public side: OrderSide,
    public variety: OrderVariety,
    public target: CoinMoney,
    public exchangeCurrency: CoinCurrency) {
  }

}

export default OrderRequest;
