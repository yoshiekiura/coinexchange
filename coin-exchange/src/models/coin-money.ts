import Joi from "joi";
import CoinCurrency from "../common/core/coin-currency";

class CoinMoney {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    currency: Joi.string().alphanum().min(3).max(3).required(),
    amount: Joi.number().required()
  });

  // Factory method.
  public static makeCoinMoney(obj: {}): CoinMoney {
    const cloned = Object.assign({}, obj) as CoinMoney;
    return cloned;
  }

  constructor(public currency: CoinCurrency, public amount: number) {
  }

}

export default CoinMoney;
