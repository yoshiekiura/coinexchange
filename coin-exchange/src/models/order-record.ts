import Joi from "joi";
import CoinCurrency from "../common/core/coin-currency";
import OrderSide from "../common/core/order-side";
import OrderStatus from "../common/core/order-status";
import CoinMoney from "./coin-money";
import OrderVariety from "./order-variety";

class OrderRecord {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    id: Joi.string().alphanum().min(1).max(64).required(),
    uid: Joi.string().alphanum().min(1).max(64).required(),
    side: Joi.number().required(),
    variety: Joi.object().keys({
      orderType: Joi.number().required(),
      rateLimit: Joi.number(),
    }),
    target: Joi.object().keys({
      currency: Joi.string().alphanum().min(3).max(3).required(),
      amount: Joi.number().required()
    }),
    remaining_amount: Joi.number(),
    exchangeCurrency: Joi.string().alphanum().min(3).max(3).required(),
    createdAt: Joi.date(),
    closedAt: Joi.date(),
    status: Joi.number(),
    transactions: Joi.array(),
  });

  // Factory method.
  public static makeOrderRecord(obj: any): OrderRecord {
    const cloned = Object.assign({}, obj) as OrderRecord;
    cloned.variety = OrderVariety.makeOrderVariety(obj.variety);
    cloned.target = CoinMoney.makeCoinMoney(obj.target);
    // const list: string[] = [];
    // for (const t of obj.transactions) {
    //   list.push(t);
    // }
    // cloned.transactions = list;
    return cloned;
  }

  constructor(
    public id: string,
    public uid: string,
    public side: OrderSide,
    public variety: OrderVariety,
    public target: CoinMoney,
    public remainingAmount: number,
    public exchangeCurrency: CoinCurrency,
    public createdAt: Date,
    public closedAt: Date,
    public status: OrderStatus,    // Now that we have a new field, remainingAmount, as well as closedAt,
                                   //   status seems a bit redundant at this point. But, just keep it for now.
    public transactions: string[]  // Not being used currently.
    ) {

  }

}

export default OrderRecord;
