import Joi from "joi";
import CoinMoney from "./coin-money";

class CurrencyWallet {

  public static schema: Joi.ObjectSchema = Joi.object().keys({
    uid: Joi.string().alphanum().min(1).max(64).required(),
    value: Joi.object().keys({
      currency: Joi.string().alphanum().min(3).max(3).required(),
      amount: Joi.number().required()
    })
  });

  // Factory method.
  public static makeCurrencyWallet(obj: any): CurrencyWallet {
    const cloned = Object.assign({}, obj) as CurrencyWallet;
    cloned.value = CoinMoney.makeCoinMoney(obj.value);
    return cloned;
  }

  constructor(public uid: string, public value: CoinMoney) {
  }

}

export default CurrencyWallet;
