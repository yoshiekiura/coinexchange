// Env helper functions.

const { NODE_ENV } = process.env;

const isDev = () => (NODE_ENV === "development") as boolean;

const isProd = () => (NODE_ENV === "production") as boolean;

const getEnv = () => (NODE_ENV) as string;

const setEnv = (env: string) => {
  process.env.NODE_ENV = env;
};

export default { isDev, isProd, getEnv, setEnv };
