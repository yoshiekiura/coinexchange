import OrderSide from "../common/core/order-side";
import OrderStatus from "../common/core/order-status";
import OrderType from "../common/core/order-type";
import InsertManager from "../db/insert-manager";
import QueryManager from "../db/query-manager";
import * as RateHandler from "../handlers/rate-handler";
import DecimalHelper from "../helpers/decimal-helper";
import ExchangeRate from "../models/exchange-rate";
import OrderRecord from "../models/order-record";

// Work in progress
// Basic market orders seem to work at this point.
// But, I haven't really tested with limit orders,
//      and with more complex trading scenarios.
// It'll likely have a lot of logical errors, at this point.
// Work in progress

// Note:
// A buy order of currency A for x amount with currency B
//    is equivalent to a buy order of currency B for y amount with currency A
//    with appropriate x and y based on exchange rates.
// Hence, we can actually partially match buy and buy orders and sell and sell orders.
// But, in this demo implementation, we will not deal with such complexities.
// We only match buy and sell orders.
// Also, this may not be the best, or even correct, implementation.
// In this algorithm, we only try to match the newly creatd orders.
// But, in practice, among the orders already stored in the DB,
//    there can be new matches
//    due to the fact that the buy/sell price/rate constantly changes.
// I'm actually not entirely sure though.
// But, I hope, this algorithm works in simple scenarios.
class TradingManager {
  static get Instance() {
    return this.instance || (this.instance = new this());
  }

  private static instance: TradingManager;

  private constructor() { }

  // Refer to the comments in QueryManager for the algorithms we use.
  // order: typically, a new order just created in the system.
  public match(order: OrderRecord) {
    // Get the current rate first.
    RateHandler.getRate(order.target.currency,
      order.exchangeCurrency, (exchangeRate: ExchangeRate) => {
        let currentRate = 1.0;   // default value.
        if (exchangeRate) {
          currentRate = exchangeRate.rate;
        }
        this.matchBasedOnCurrentRate(order, currentRate);
      });
  }

  private matchBasedOnCurrentRate(order: OrderRecord, currentRate: number) {
    // // What to do??? This is probably an error, and we should just bail out.
    // if (currentRate === 0 || !isFinite(currentRate)) {
    //   console.log(`Invalid currentRate = ${currentRate}. Resetting it to 1, for now.`);
    //   currentRate = 1.0;
    // }

    // Note: target and exchange are reversed when we try to find a match.
    const matchSide = (order.side === OrderSide.Buy) ? OrderSide.Sell : OrderSide.Buy;
    const matchTargetCurrency = order.exchangeCurrency;
    const matchExchangeCurrency = order.target.currency;

    // try to find a match
    if (order.variety.orderType === OrderType.Market) {
      QueryManager.findBestMatchingOrderForMarketOrder(1 / currentRate,
        matchSide,
        matchTargetCurrency,
        matchExchangeCurrency,
        (result: any, rate: number) => {
          console.log(`Market order: Match rate = ${rate}`, result);
          if (!result || rate === 0) {
            console.log("No matching record found for market order:", order);
            return;
          }
          const matchingOrder = OrderRecord.makeOrderRecord(result);
          this.matchTwoOrders(order, matchingOrder, 1 / rate);
        });
    } else {
      QueryManager.findBestMatchingOrderForLimitOrder(1 / currentRate,
        matchSide,
        matchTargetCurrency,
        matchExchangeCurrency,
        1 / order.variety.rateLimit,
        (result: any, rate: number) => {
          console.log(`Limit order: Match rate = ${rate}`, result);
          if (!result || rate === 0) {
            console.log("No matching record found for limit order:", order);
            return;
          }
          const matchingOrder = OrderRecord.makeOrderRecord(result);
          this.matchTwoOrders(order, matchingOrder, 1 / rate);
        });
    }
  }

  private matchTwoOrders(order: OrderRecord, matchingOrder: OrderRecord, tradingRate: number) {
    console.log("order = ", order);
    console.log("matchingOrder = ", matchingOrder);
    console.log(`Rate = ${tradingRate}`);

    // // What to do??? This is probably an error, and we should just bail out.
    // if (tradingRate === 0 || !isFinite(tradingRate)) {
    //   console.log(`Invalid tradingRate = ${tradingRate}. Resetting it to 1, for now.`);
    //   tradingRate = 1.0;
    // }

    const now = new Date();

    let buyOrder: OrderRecord;
    let sellOrder: OrderRecord;
    let buyRate: number;
    if (order.side === OrderSide.Buy) {
      buyOrder = order;
      sellOrder = matchingOrder;
      buyRate = tradingRate;
    } else {
      buyOrder = matchingOrder;
      sellOrder = order;
      buyRate = 1 / tradingRate;
    }

    const buyOrderId = buyOrder.id;
    const sellOrderId = sellOrder.id;
    const buyerId = buyOrder.uid;
    const sellerId = sellOrder.uid;
    const buyCurrency = buyOrder.target.currency;
    // const buyAmount = buyOrder.target.amount;
    const buyAmount = buyOrder.remainingAmount;
    const sellCurrency = sellOrder.target.currency;
    // const sellAmount = sellOrder.target.amount;
    const sellAmount = sellOrder.remainingAmount;
    const completedAt = now;

    const buyEquivalent = DecimalHelper.adjustPrecision(sellAmount * buyRate);
    const sellEquivalent = DecimalHelper.adjustPrecision(buyAmount / buyRate);
    let orderFilled = false;
    let matchOrderFilled = false;
    let buyTransactionAmount = 0;
    let sellTransactionAmount = 0;

    if ((buyAmount === buyEquivalent)   // Perfect match
      || (order.side === OrderSide.Buy) && (buyAmount < buyEquivalent)
      || (order.side === OrderSide.Sell) && (buyAmount > buyEquivalent)) {
      // Complete fill
      orderFilled = true;
      matchOrderFilled = (buyAmount === buyEquivalent) ? true : false;
    } else {
      // Partial fill
      orderFilled = false;
      matchOrderFilled = true;
    }

    // TBD: Does this make sense? Need to double check.
    buyTransactionAmount = (buyAmount <= buyEquivalent) ? buyAmount : buyEquivalent;
    sellTransactionAmount = (buyAmount >= buyEquivalent) ? sellAmount : sellEquivalent;

    // TBD:
    // These all should be done within a DB transaction.
    // For simplicity, we will omit it here.
    // (The library I happened to choose for this project, node-sqlite3,
    //     has a terrible support for transaction.
    // I don't care much for their callback-based API either.
    // For the next node.js app project, I'll probably use a different driver library,
    // or, even high-level ORM packages like squelize.)

    // record it on db, on the transactions table
    // and, adjust orders and user_wallet.

    InsertManager.createTransaction(buyOrderId,
      sellOrderId,
      buyerId,
      sellerId,
      buyCurrency,
      buyTransactionAmount,
      sellCurrency,
      sellTransactionAmount,
      completedAt,
      (transactionId: string) => {

        // Update orders
        // TBD: Not thoroughly tested.

        // tbd:
        let orderRemainingAmount = order.remainingAmount;
        let matchOrderRemainingAmount = matchingOrder.remainingAmount;

        let orderClosedAt: Date = null;
        let orderStatus = order.status;
        if (orderFilled) {
          orderClosedAt = now;
          orderStatus = OrderStatus.Completed;
          orderRemainingAmount = 0;
        } else {
          // tbd: ???
          orderRemainingAmount = Math.max(0, order.remainingAmount
            - ((order.side === OrderSide.Buy) ? buyTransactionAmount : sellTransactionAmount));
        }

        let matchOrderClosedAt: Date = null;
        let matchOrderStatus = matchingOrder.status;
        if (matchOrderFilled) {
          matchOrderClosedAt = now;
          matchOrderStatus = OrderStatus.Completed;
          matchOrderRemainingAmount = 0;
        } else {
          // tbd: ???
          matchOrderRemainingAmount = Math.max(0, matchingOrder.remainingAmount
            - ((matchingOrder.side === OrderSide.Buy) ? buyTransactionAmount : sellTransactionAmount));
        }

        InsertManager.updateOrder(order.id,
          orderRemainingAmount,
          orderClosedAt,
          orderStatus, (suc: boolean) => {
            // tbd:
            console.log(`updateOrder(${order.id}): suc = ${suc}`);
          });

        InsertManager.updateOrder(matchingOrder.id,
          matchOrderRemainingAmount,
          matchOrderClosedAt, matchOrderStatus, (suc: boolean) => {
            // tbd:
            console.log(`updateOrder(${matchingOrder.id}): suc = ${suc}`);
          });

        // TBD: Update user_wallet
        //      Again, the whole exchange transaction,
        //      including creating a new transaction record, updating orders,
        //      and updating user wallets, all should happen in a DB transaction!!!

        InsertManager.addToUserWallet(buyerId, buyCurrency, buyTransactionAmount, (suc: boolean) => {
          // tbd:
          console.log(`addToUserWallet(${buyerId}, ${buyCurrency}): suc = ${suc}`);
        });
        InsertManager.addToUserWallet(buyerId, sellCurrency, -sellTransactionAmount, (suc: boolean) => {
          // tbd:
          console.log(`addToUserWallet(${buyerId}, ${sellCurrency}): suc = ${suc}`);
        });
        InsertManager.addToUserWallet(sellerId, sellCurrency, sellTransactionAmount, (suc: boolean) => {
          // tbd:
          console.log(`addToUserWallet(${sellerId}, ${sellCurrency}): suc = ${suc}`);
        });
        InsertManager.addToUserWallet(sellerId, buyCurrency, -buyTransactionAmount, (suc: boolean) => {
          // tbd:
          console.log(`addToUserWallet(${sellerId}, ${buyCurrency}): suc = ${suc}`);
        });

        // Try to find more matches.
        if (!orderFilled) {
          setTimeout(() => {
            TradingManager.Instance.match(order);
          }, 0);
        }
      });
  }

  // private updateUserWallet() {
  // }

}

export default TradingManager;
