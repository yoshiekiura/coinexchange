import express, { NextFunction, Request, Response } from "express";

import * as DatabaseHandler from "../../handlers/dev/database-handler";
import ENV from "../../util/env";

const router = express.Router();

///////////////////////////////////////////////////////
// For dev purposes only.

// Load sample data fixtures.
router.route("/fixtures").post((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);

  // tbd:
  const suc = DatabaseHandler.loadFixtures();
  if (suc) {
    res.status(204).send("");
  } else {
    res.status(500).send("Failed to load fixtures.");
  }
});

// Exports the current db schema/data.
router.route("/export").post((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);

  // tbd:
  const suc = DatabaseHandler.doExport();
  if (suc) {
    res.status(200).json({});
  } else {
    res.status(500).send("Failed to explort data.");
  }
});

export default router;
