import express, { NextFunction, Request, Response } from "express";

// import * as AdminHandler from "../handlers/admin-handler";
import ENV from "../util/env";
import dbRouter from "./dev/db";

const router = express.Router();

// Get House Balance endpoint
router.route("/balance").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);

  // const response = AdminHandler.getBalance();
  // res.json(response);

  res.status(500).json({ error: `admin/balance API is not implemented` });
  return;
});

// For dev purposes only.
router.use("/db", dbRouter);

export default router;
