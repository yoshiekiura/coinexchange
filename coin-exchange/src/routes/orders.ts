import express, { NextFunction, Request, Response } from "express";
import Joi from "joi";

import CoinCurrency from "../common/core/coin-currency";
import OrderType from "../common/core/order-type";
import CurrencyRegsitry from "../common/currency-registry";
import * as OrderHandler from "../handlers/order-handler";
import DecimalHelper from "../helpers/decimal-helper";
import OrderRecord from "../models/order-record";
import OrderRequest from "../models/order-request";
import ENV from "../util/env";

const router = express.Router();

// Place an order.
router.route("/").post((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);

  const body = req.body;
  if (!body) {
    res.status(400).json({ error: "Request body is missing." });
    return;
  }
  const validated = Joi.validate(body, OrderRequest.schema);
  if (validated.error) {
    res.status(400).json({ error: validated.error.details[0].message });
    return;
  }
  const order = validated.value as OrderRequest;

  // TBD: toUpperCase() ??
  const targetCurrency = order.target.currency;
  if (!CurrencyRegsitry.isValid(targetCurrency)) {
    res.status(400).json({
      error: `Invalid currency: targetCurrency = ${targetCurrency}`
    });
    return;
  }
  const targetAmount = order.target.amount;
  if (!DecimalHelper.isValidPrecision(targetAmount)) {
    res.status(400).json({ error: `Invalid targetAmount = ${targetAmount}` });
    return;
  }
  const rateLimit = order.variety.rateLimit;
  if (order.variety.orderType === OrderType.Limit && rateLimit < Number.EPSILON) {
    res.status(400).json({ error: `Invalid limit order:  = ${rateLimit}` });
    return;
  }

  OrderHandler.createOrder(order, (record: OrderRecord) => {
    if (record) {
      res.json(record);
    } else {
      res.status(500).json({ error: `Failed to add to an order for uid = ${order.uid}` });
    }
  });
});

// Order
router.route("/:orderId").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const orderId = req.params.orderId;
  console.log(`orderId = ${orderId}`);

  OrderHandler.getOrder(orderId, false, (record: OrderRecord) => {
    console.log("record = ", record);
    if (record) {
      res.json(record);
    } else {
      res.status(404).json({ error: `Order not found for orderId = ${orderId}` });
    }
  });
});

// Open orders
router.route("/:targetCurrency/:exchangeCurrency").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const targetCurrency = (req.params.targetCurrency as string).toUpperCase();
  const exchangeCurrency = (req.params.exchangeCurrency as string).toUpperCase();
  console.log(`targetCurrency = ${targetCurrency}`);
  console.log(`exchangeCurrency = ${exchangeCurrency}`);

  if (!CurrencyRegsitry.isValid(targetCurrency) || !CurrencyRegsitry.isValid(exchangeCurrency)) {
    res.status(400).json({
      error: `Invalid currencies: targetCurrency = ${targetCurrency}, exchangeCurrency = ${exchangeCurrency}`
    });
    return;
  }
  if (targetCurrency === exchangeCurrency) {
    res.status(400).json({ error: `Cannot place an order with the same currency: ${targetCurrency}` });
    return;
  }

  // tbd:
  OrderHandler.getOpenOrdersForCurrencies(targetCurrency as CoinCurrency,
    exchangeCurrency as CoinCurrency, (records: OrderRecord[]) => {
      if (records) {
        res.json(records);
      } else {
        res.status(404).json({
          error: `No open orders found for targetCurrency = ${targetCurrency}
            and exchangeCurrency = ${exchangeCurrency}`
        });
      }
    });
});

export default router;
