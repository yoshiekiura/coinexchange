import express from "express";
import path from "path";
const router = express.Router();

import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";

// import swaggerDocument from "../swagger.json";
const swaggerDocument = YAML.load(path.join(__dirname, "/../swagger.yaml"));

router.use("/", swaggerUi.serve);
router.get("/", swaggerUi.setup(swaggerDocument));

export default router;
