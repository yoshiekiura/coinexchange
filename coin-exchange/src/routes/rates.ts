import express, { NextFunction, Request, Response } from "express";

import CoinCurrency from "../common/core/coin-currency";
import CurrencyRegsitry from "../common/currency-registry";
import * as RateHandler from "../handlers/rate-handler";
import ExchangeRate from "../models/exchange-rate";
import ENV from "../util/env";

const router = express.Router();

// Exchange Rates endpoint
router.route("/:targetCurrency/:baseCurrency").get((req: Request, res: Response, next: NextFunction) => {
  // console.log("req = ", req);
  // console.log("res = ", res);
  const targetCurrency = (req.params.targetCurrency as string).toUpperCase();
  const baseCurrency = (req.params.baseCurrency as string).toUpperCase();
  console.log(`targetCurrency = ${targetCurrency}`);
  console.log(`baseCurrency = ${baseCurrency}`);

  if (!CurrencyRegsitry.isValid(targetCurrency) || !CurrencyRegsitry.isValid(baseCurrency)) {
    res.status(400).json({
      error: `Invalid currencies: targetCurrency = ${targetCurrency}, baseCurrency = ${baseCurrency}`
    });
    return;
  }
  if (targetCurrency === baseCurrency) {
    res.status(200).json(new ExchangeRate(targetCurrency as CoinCurrency,
      targetCurrency as CoinCurrency, 1.0, new Date()));
    return;
  }

  RateHandler.getRate(targetCurrency as CoinCurrency,
    baseCurrency as CoinCurrency, (record: ExchangeRate) => {
      if (record) {
        res.json(record);
      } else {
        res.status(404).json({
          error: `No exchange rate found for targetCurrency = ${targetCurrency}
            and baseCurrency = ${baseCurrency}`
        });
      }
    });
});

export default router;
