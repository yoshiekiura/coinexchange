// Sample data

const sqlInsertIntoUsers = [
  `INSERT INTO users (id, name) VALUES ('1', 'user01');`,
  `INSERT INTO users (id, name) VALUES ('2', 'user02');`,
  `INSERT INTO users (id, name) VALUES ('3', 'user03');`,
];

const sqlInsertIntoUserWallet = [
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('1', 'USD', 10000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('1', 'BTC', 20000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('1', 'ETH', 30000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('1', 'RBX', 40000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('1', 'XLM', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('2', 'USD', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('2', 'BTC', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('2', 'ETH', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('2', 'RBX', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('3', 'USD', 50000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('3', 'BTC', 40000);`,
  `INSERT INTO user_wallet (uid, currency, amount) VALUES ('3', 'XLM', 30000);`,
];

const getSqlForFixtures = () => {
  return sqlInsertIntoUsers.concat(sqlInsertIntoUserWallet);
};

export default { getSqlForFixtures };
