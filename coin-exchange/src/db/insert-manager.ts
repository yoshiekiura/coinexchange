import * as uuid from "uuid";
import CoinCurrency from "../common/core/coin-currency";
import OrderSide from "../common/core/order-side";
import OrderStatus from "../common/core/order-status";
import DatabaseHelper from "../helpers/database-helper";
import CoinMoney from "../models/coin-money";
import OrderVariety from "../models/order-variety";

class InsertManager {

  // TBD: Now that we added a new function, updateUserWallet(),
  //      we can probably refactor this to call that function.
  public static addToUserWallet(uid: string,
                                currency: CoinCurrency,
                                delta: number,
                                cb: (suc: boolean) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        // db.exec("BEGIN TRANSACTION");

        // Upsert uid into the users table first.
        const stmt1 = `INSERT INTO users (id)
          VALUES ('${uid}')
          ON CONFLICT(id) DO NOTHING`;
        db.exec(stmt1, (err1) => {
          if (err1) {
            console.log("Error 1 = ", err1);
            cb(false);
          } else {
            // TBD:
            // We need to check so that the resulting amount stays non-negative.
            const stmt2 = `INSERT INTO user_wallet (uid, currency, amount)
            VALUES ('${uid}', '${currency}', ${delta})
            ON CONFLICT(uid, currency) DO UPDATE SET amount = amount + ${delta}`;
            db.exec(stmt2, (err2) => {
              if (err2) {
                console.log("Error 2 = ", err2);
                cb(false);
              } else {
                cb(true);
              }
            });
          }
        });

        // db.exec("COMMIT");
      });
    } catch (ex) {
      console.log("Failed to run insert.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static updateUserWallet(uid: string,
                                 currency: CoinCurrency,
                                 amount: number,   // amount >= 0
                                 cb: (suc: boolean) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        // db.exec("BEGIN TRANSACTION");

        // Upsert uid into the users table first.
        const stmt1 = `INSERT INTO users (id)
          VALUES ('${uid}')
          ON CONFLICT(id) DO NOTHING`;
        db.exec(stmt1, (err1) => {
          if (err1) {
            console.log("Error 1 = ", err1);
            cb(false);
          } else {
            const stmt2 = `INSERT INTO user_wallet (uid, currency, amount)
            VALUES ('${uid}', '${currency}', ${amount})
            ON CONFLICT(uid, currency) DO UPDATE SET amount = ${amount}`;
            db.exec(stmt2, (err2) => {
              if (err2) {
                console.log("Error 2 = ", err2);
                cb(false);
              } else {
                cb(true);
              }
            });
          }
        });

        // db.exec("COMMIT");
      });
    } catch (ex) {
      console.log("Failed to run insert.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  public static createOrder(uid: string,
                            side: OrderSide,
                            variety: OrderVariety,
                            target: CoinMoney,
                            exchangeCurrency: CoinCurrency,
                            createdAt: Date,
                            status: OrderStatus,
                            cb: (orderId: string) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        // db.exec("BEGIN TRANSACTION");

        const id = uuid.v4();
        console.log(`createOrder() id = ${id}`);
        const stmt = `INSERT INTO orders
          (id, uid, side, order_type, rate_limit,
            target_currency, target_amount, remaining_amount,
            exchange_currency, created_at, status)
          VALUES ('${id}', '${uid}', ${side}, ${variety.orderType}, ${variety.rateLimit},
            '${target.currency}', ${target.amount}, ${target.amount},
            '${exchangeCurrency}', '${createdAt}', ${status})`;
        db.exec(stmt, (err) => {
          if (err) {
            console.log("Error = ", err);
            cb(null);
          } else {
            cb(id);
          }
        });

        // db.exec("COMMIT");
      });
    } catch (ex) {
      console.log("Failed to run insert.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  // Only the following three fields are updatable in an existing order.
  public static updateOrder(id: string,
                            remainingAmount: number,
                            closedAt: Date,
                            status: OrderStatus,
                            cb: (suc: boolean) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        // db.exec("BEGIN TRANSACTION");

        console.log(`updateOrder() id = ${id}`);
        const stmt = `UPDATE orders SET
            remaining_amount = ${remainingAmount},
            closed_at = '${closedAt}', status = ${status}
          WHERE id = '${id}'`;
        db.exec(stmt, (err) => {
          if (err) {
            console.log("Error = ", err);
            cb(false);
          } else {
            cb(true);
          }
        });

        // db.exec("COMMIT");
      });
    } catch (ex) {
      console.log("Failed to run insert.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  // Transaction records are immutable.
  //   There is no updateTransaction().
  public static createTransaction(buyOrderId: string,
                                  sellOrderId: string,
                                  buyerId: string,
                                  sellerId: string,
                                  buyCurrency: CoinCurrency,
                                  buyAmount: number,
                                  sellCurrency: CoinCurrency,
                                  sellAmount: number,
                                  completedAt: Date,
                                  cb: (transactionId: string) => void) {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        // db.exec("BEGIN TRANSACTION");

        const id = uuid.v4();
        console.log(`createTransaction() id = ${id}`);
        const stmt = `INSERT INTO transactions
            (id, buy_order_id, sell_order_id, buyer_id, seller_id,
            buy_currency, buy_amount, sell_currency, sell_amount,
            completed_at)
          VALUES ('${id}', '${buyOrderId}', '${sellOrderId}', '${buyerId}', '${sellerId}',
            '${buyCurrency}', ${buyAmount}, '${sellCurrency}', ${sellAmount},
            '${completedAt}')`;
        db.exec(stmt, (err) => {
          if (err) {
            console.log("Error = ", err);
            cb(null);
          } else {
            cb(id);
          }
        });

        // db.exec("COMMIT");
      });
    } catch (ex) {
      console.log("Failed to run insert.", ex);
    } finally {
      // DatabaseHelper.Instance.close();
    }
  }

  private constructor() { }
}

export default InsertManager;
