import DatabaseHelper from "../helpers/database-helper";
import FIXTURES from "./fixtures";
import SCHEMA from "./schema";

class DatabaseManager {

  public static createTables(): boolean {
    try {
      const db = DatabaseHelper.Instance.DB;
      db.serialize(() => {
        for (const s of SCHEMA.getSqlForCreateWithoutUserForeignKeys()) {
          db.run(s);
        }

        // // Quick sanity check
        // console.log("Performing a quick DB test.");
        // const stmt1 = db.prepare("INSERT INTO users VALUES (?,?)");
        // for (let i = 0; i < 5; i++) {
        //   stmt1.run(`user-${i}`, `user-${i}`);
        // }
        // stmt1.finalize();
        // db.each("SELECT id, name FROM users", (err, row) => {
        //   console.log(`Created and deleted: ${row.id} - ${row.name}`);
        // });
        // db.exec("DELETE FROM users");

        // To make testing a bit easier,
        // We load some sample user data.
        console.log(`Loading sample data...`);
        for (const f of FIXTURES.getSqlForFixtures()) {
          db.run(f, () => {
            // console.log(`Data loaded: ${f}`);
          });
        }
        setTimeout(() => {
          console.log(`...Sample data loaded.`);
        }, 3000);
      });
    } catch (ex) {
      console.log("Failed to create all tables.", ex);
      return false;
    } finally {
      // DatabaseHelper.Instance.close();
    }
    return true;
  }

  private constructor() { }

}

export default DatabaseManager;
