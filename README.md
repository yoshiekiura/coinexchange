# Coin Exchange

## Problem

Build a "currency exchange" service, in which users can buy and sell one currency
in exchange with other currency.



## Use Cases

1. User deposits fund in a certain currency. (For simplicity, we won't support withdrawals.)
1. User can view his/her balance.
1. User can view the last transaction prices between two currencies.
1. User can place an order.
    * An order can be one of two sides, buy and sell, and it can be one of two types, market order or limit order.
    * Once two matching orders are found, the orders are settled and the two users' balances are adjusted.
1. For simplicity, once an order is created, it cannot be updated or canceled.
1. User can view his/her currently open orders.
1. User can view all open orders in the system, between two currencies.
1. User can view his/her past transaction records.




## Domain Modeling

* Set of supported currencies. A currency is represented by a sequence of three alphabets. For now we will support 5 currencies: USD, RBX, BTC, ETH, and XLM.
* Users: Id. For simplicity. We will not store users on our system. We will support any user with a valid (random) ID (e.g., UUID).
* User balance: Each user will have a balance associated with each currency. E.g., User Id, Currency, Balance.
* Order record: We need to keep track of all orders, open and settled. E.g., Order user, Order type, Order side, Bid/Ask price (if limit order), Original amount, Remaining/unfulfilled amount, Source currency, Target currency, Status (Open/Partially filled/Settled), Order date, Settled date, ... 
* Transaction record: We keep track of all partial fulfillment. E.g., OrderId, Source User, Target User, Source Currency/amount, Target Currency/amount, House profit, Transaction time, ...
* House balance: We can deduce the house balance from the transaction record, and hence we won't need a separate model for house balance.




## API Design (Draft)

> This is the initial design draft. The current WIP version is available here: https://codingdrill.gitlab.io/coinexchange/ 


### User Balance - Wallet

```
GET /users/:uid/wallet

Response: { uid, balance: [ { currency, amount } ] }
```

```
POST /users/:uid/wallet

Request: { uid, deposit: { currency, amount } }
Response: { uid, balance: { currency, amount } }
```


<!-- ```
GET /users/:uid/wallet?cur=:currency

Response: { uid, currency, totalAmount }
``` -->




### Coin Pair Exchange Rates

```
GET /rates/:currencyA/:currencyB

Response: { CurrencyA, CurrencyB, Exchnage Rate }
```



### Orders

```
POST /orders

Request: { uid, orderSide, orderType: { type, limitPrice }, target: { currency, amount }, exchangeCurrency }
Response: { orderId, status, createdTime }
```

```
GET /orders/:orderId

Response: { orderId, status, orderSide, orderType: { type, limitPrice }, target: { currency, amount }, exchangeCurrency, createdTime, [ { transactionId, transactionTime } ] }
```

```
GET /orders/:currencyA/:currencyB?status=open

Response: { orderId, status, orderSide, orderType: { type, limitPrice }, target: { currency, amount }, exchangeCurrency }
```

```
GET /users/:uid/orders

Response: { [ orderId, status, orderSide, orderType: { type, limitPrice }, target: { currency, amount }, exchangeCurrency, createdTime, [ { transactionId, transactionTime } ] ] }
```



### Transactions

```
GET /transactions/:transactionId

Response: { transactionId, orderId, userA, userB, currencyA/amount, currencyB/amount, transactionTime }
```


```
GET /users/:uid/transactions

Response: { uid, [ transactionId, orderId, userB, currencyA/amount, currencyB/amount, transactionTime ] }
```



### House Balance

```
GET /balance


Response: { { currency: amount } }
```




## DB Schema Design


> This was the initial design. The current WIP version is available under `/src/db/`.


### User

* id
* name

_Not really needed. To be used to set foreign key constraint._


### User Wallet

* uid
* currency
* amount


### Orders

* id
* uid
* side
* type
* limitRate
* targetCurrency
* targetAmount
* exchangeCurrency
* createdAt
* closedAt
* status


### Transactions

* id
* orderId
* buyerId
* sellerId
* buyCurrency
* buyAmount
* sellCurrency
* sellAmount
* completedAt


### House Crumbs/Treasure

* transactionId
* buyCurrency
* buyDelta
* sellCurrency
* sellDelta
* completedAt

_Not being used._



## Brief Introduction

_This readme does not seem very well organized/structured, but it is actually chronological. I started from requirement/use cases, and I did some API design and data modeling next. Then, I proceeded with implementation._


* I first wrote a swagger doc for the API so that I can get any feedback, if needed. The swagger doc is hosted as a standalone react app (using `swagger-ui-react`) on Gitlab Pages: https://codingdrill.gitlab.io/coinexchange/
* I scaffolded this Express app using `express-generator`.
* Then, I converted the project into Typescript, primarily because I wanted to use ES6 features. As it turned out, it seemed a bit easier to convert the scaffolded app into typescript project (which was a breeze) than into an ES6 project.
* I first implementated all routes (in Express).
* I decided to use sqlite for data storage (for proof of concept), and I chose the `node-sqlite3` driver, which turned out be not a very good library. First of all, javascript community moved on to Promises and async/await (as far as I'm concerned). This library still uses callback-based API. Also, I found it extremely hard to implement DB transactions within their framework. (This demo does not use transaction.) I'll definitely consider a different library (including a high-level ORM like squelize) for the next node.js/Express project. 
* Currently, the database instance is created in memory only. 
* I created database schema and put it in .ts file (rather than .sql file) for convenience. I also created some minimal data fixture (for user wallet) so that trading can be tested without having to add money to the users' wallet first, which can be time-consuming when you do it through API.
* I then implementated all database queries, in the low level SQL. (That is, no ORM).
* Next, I created handlers which connect routes and database queries. At this point, the service was fully functional as a Web services. You can insert rows, and fetch them as long as they are supported by APIs, and they all worked very well.
* The final step was implementing the trading logic. This was the hardest part, as it turned out. I am sure there are still lots of bugs. But, the nice thing is _it works_ (at least, for some simple scenarios). This is the current and final stage.
* I'll likely get back to this project and "finish" it some time in the next few weeks, possibly. I'll need to spend some time doing some extensive testing first.



## Scenarios

Trading can comprise multiple users and multiple currencies/coin types, and it can get very complicated.
As of today, I tested the following two scenarios:

### Secenario 1

* User "1" places a market order to buy 100 USD with BTC.
* User "2" places a market order to buy 100 BTC with USD.
* At this point (since there has been no transactions), the exchange rate between USD and BTC is 1.0 (default). The trading will happen between these two orders, and the transaction will be recorded in the transactions table. The wallets of Users "1" and "2" are correctly updated.


### Secenario 2

* User "1" places a market order to buy 100 USD with BTC.
* User "2" places a market order to buy 50 BTC with USD.
* The trading will happen between these two orders, and User 1's order will be partially filled.
* User "2" places another market order to buy 50 BTC with USD.
* At this point, User 1's remaining order will be filled by User 2's new order, and their walets will be accordingly adjusted.




## How to Build and Run the App

This is a standard Express app (except that it is mostly written in Typescript).
You can use npm scripts to build, test, and run the app.

To build:
```
npm run build
```

To test (there are a small number of unit tests available, at this point):
```
npm run test
```

To run:
```
npm run start
```

You can also do `npm run dev:start` to build and run.

When you run the app, the root route "/" is currently set to the Swagger doc (using `swagger-ui-express`). Hence you can easily test other endpoints using this swagger UI ("Try It Out"). Unfortunately, somewhat obscure part is using enum types. At the bottom of the swagger page, you can find the enum definitions.



